const router = require('express').Router()
const axios = require('axios')
// const { BASE_URL } = 'http://localhost:3030/'
const { BASE_URL } = require('./config/constants')

router.get('/feeds', (req, res) => {
    axios.get(`${BASE_URL}/mainfeed`)
    .then(result => {
        // console.log(result)
        res.render('feed', {
            post: result.data.data
        })
    })
})

router.get('/post/:id', (req, res) => {
    axios.get(`${BASE_URL}/post/${req.params.id}`)
    .then(result => {
        res.render('viewPost', {
            post: result.data.data
        })
    })
})

router.get('/', (req, res) => {
    axios.get(`${BASE_URL}/mainfeed`)
    .then(result => {
        res.render('main', {
            post: result.data.data
        })
    })
})

// router.post('/post/create', (req, res) => {
//     axios.post(`${BASE_URL}/post`, req.body)
//     .then(result => {
//         res.render('create', {
//             post: result.data.data
//         })
//     }).catch(error => {
//         res.render('create', {
//             errors: error
//         })
//     })
// })

router.post(`/authIn`, (req, res) => {
    let { email, password } = req.body;
    let message = ``;
    console.log(email, password);
    axios
      .post(`${BASE_URL}/login`, {
        email: email,
        password: password,
      })
      .then(
        (response) => {
          if (response.data != `wrong email`) {
            if (response.data != `wrong password`) {
            //   console.log(`welcome ${username}`);
              res
                .status(200)
                .cookie("login_token")
                .redirect(`/feeds`);
            } else {
              message = "wrong Password";
              console.log(`Wrong Password`);
            //   res.status(401).render(`auth/index.ejs`, { message: message });
            }
          } else {
            message = "wrong email";
            console.log(`wrong email`);
            // res.status(401).render(`auth/index.ejs`, { message: message });
          }
        },
        (error) => {
          console.log(error);
        //   res.status(403).redirect(`/signin`);
        }
      );
  });

router.get("/signout", (req, res) => {
    res.clearCookie("login_token").redirect(`/`);
}); 

router.post("/", (req, res) => {
    axios.post(`${BASE_URL}/register`, req.body)
    .then(result => {
      res.redirect("#registSuccess")
    })
    .catch(err => console.log(err))
})

router.get('/saved', (req, res) => {
    res.render('saved')
})

router.get('/feeds', (req, res) => {
    res.render('feed')
})

router.get('/feeds/create-post', (req, res)=>{
    res.render('create')
})

router.get('/register', (req, res) => {
    res.render('register')
})

router.get('/feeds/user/view-post', (req, res)=>{
    res.render('viewPost')
})

router.get('/feeds/profile-not-complete', (req, res)=>{
    res.render('registNotComplete')
})

router.get('/setting', (req, res) => {
    res.render('setting')
})

router.get('/profile', (req, res) => {
    res.render('profile')
})

module.exports = router 