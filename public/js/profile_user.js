const editBtn = document.querySelector(".editProfile")
const span = document.getElementsByClassName("close")[0];
const modal = document.querySelector(".modal")

editBtn.onclick = function() {
  modal.style.display = "block";
}

span.onclick = function() {
  modal.style.display = "none";
}

window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}