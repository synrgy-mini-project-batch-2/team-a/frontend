const accSecurity = document.querySelector(".accSecurity")
const privacy = document.querySelector(".privacy")
const notifications = document.querySelector(".notifications")
const accEmail = document.querySelector(".boxsub")
const accPassword = document.querySelector(".boxSettingsPassword")
const accLanguage = document.querySelector(".boxSettingsLanguage")
const accProfpict = document.querySelector(".boxSettingsProfpict")
const accCloseacc = document.querySelector(".boxSettingsCloseacc")
const changeEmail = document.querySelector(".changeEmail")
const changeSubemail = document.querySelector(".changeSubemail")
const changePassword = document.querySelector(".changePassword")
const changeSubpassword = document.querySelector(".changeSubpassword")
const changeLanguage = document.querySelector(".changeLanguage")
const changeProfpict = document.querySelector(".changeProfpict")
const changeCloseacc = document.querySelector(".changeCloseacc")
const changeSubCloseacc = document.querySelector(".changeSubCloseacc")
const modal = document.querySelector(".modal")
const saveBtn = document.querySelector(".saveButton")
const span = document.getElementsByClassName("close")[0];
const currEmail = document.querySelector(".currEmail")
const email = document.querySelector("#email")

function accSecurityFunction() {
    accSecurity.style.display = "block";
    privacy.style.display = "none";
    notifications.style.display = "none";
}

function privacyFunction() {
    accSecurity.style.display = "none";
    privacy.style.display = "block";
    notifications.style.display = "none";
}

function notificationsFunction() {
    accSecurity.style.display = "none";
    privacy.style.display = "none";
    notifications.style.display = "block";
}

$(document).ready(function(){
  $(changeEmail).click(function(){
    $(accEmail).toggle();
  });
});

$(document).ready(function(){
  $(changeSubemail).click(function(){
    $(accEmail).toggle();
  });
});

$(document).ready(function(){
  $(changePassword).click(function(){
    $(accPassword).toggle();
  });
});

$(document).ready(function(){
  $(changeLanguage).click(function(){
    $(accLanguage).toggle();
  });
});

$(document).ready(function(){
  $(changeProfpict).click(function(){
    $(accProfpict).toggle();
  });
});

$(document).ready(function(){
  $(changeCloseacc).click(function(){
    $(accCloseacc).toggle();
  });
});

$(document).ready(function(){
  $(changeSubCloseacc).click(function(){
    $(accCloseacc).toggle();
  });
});

function myFunction() {
  email.style.display = "block";
  currEmail.style.display = "none";
  email.placeholder = currEmail.innerHTML;
}

saveBtn.onclick = function() {
  modal.style.display = "block";
}

span.onclick = function() {
  modal.style.display = "none";
}

window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}