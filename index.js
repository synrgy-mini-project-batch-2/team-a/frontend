const express = require('express')
const app = express()
const router = require('./router')
const bodyParser = require('body-parser');

app.use('/public', express.static('public'))
app.use('/css', express.static(__dirname + 'public/css'))
app.use('/img', express.static(__dirname + 'public/img'))
app.use('/js', express.static(__dirname + 'public/js'))

app.set('view engine', 'ejs')

app.use(express.urlencoded({ extended: true}))
app.use(bodyParser.json());

app.use(router)

app.listen(3030, () => {console.log("Server ready!")
})
